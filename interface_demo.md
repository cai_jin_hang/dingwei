### 拟合以及外扩接口
* 输入：
```json
{
    "interval": 100,
    "extend_width": 5,
    "points": [[1, 2], [2, 3], [33, 2]]
}
```
> 说明：
interval: 微信小程序采集位置的时间间隔，以毫秒为单位
extend_width: 外扩的宽度，以米为单位
points: 采集的位置信息，经纬度

* 输出：
```json
{
    "vertex_points": [[1, 2], [2, 3], [33, 2]],
    "extended_vertex_points": [[1, 2], [2, 3], [33, 2]]
}
```
> 说明：
vertex_points: 计算后的多边形的顶点经纬度，没有外扩
extended_vertex_points: 外扩后的多边形的顶点经纬度

### 判断人与建筑物位置关系以及求最短距离接口
* 输入：
```json
{
    "point": [1, 2],
    "vertex_points": [[1, 2], [2, 3], [33, 2]]
}
```
> 说明：
point: 人的位置经纬度
vertex_points: 建筑物顶点经纬度

* 输出：
```json
{
    "outer_flag": true,
    "min_dis": 10
}
```
> 说明：
outer_flag: 人是否在建筑物外边
min_dis: 最短距离，以米为单位，若人在建筑物里面，min_dis值为0
