package com.king.alg;

import com.king.constant.Constant;
import com.king.pojo.Point;

import java.util.LinkedList;
import java.util.List;

public class GPSAlgorithms {

    /**
     * 将gps点列转换成坐标点列
     *
     * @param gpsList    gps点列
     * @param firstPoint 第一个点
     * @return 坐标点列
     */
    public static List<Point> gps2Coor(List<? extends Point> gpsList, Point firstPoint) {
        List<Point> result = new LinkedList<>();
        List<Point> pointListTrans = new LinkedList<>();

        for (Point p : gpsList) {
            pointListTrans.add(new Point(p.getX() - firstPoint.getX(), p.getY() - firstPoint.getY()));
        }

        for (int i = 0; i < pointListTrans.size(); i++) {
            result.add(new Point(Constant.K * pointListTrans.get(i).getX(), Constant.K * pointListTrans.get(i).getY(), i));
        }

        return result;
    }

    /**
     * 将坐标点列转换成gps点列
     *
     * @param coor 坐标点列
     * @param ori  原来的第一个点的gps
     * @return gps点列
     */
    public static List<Point> coor2Gps(List<? extends Point> coor, Point ori) {
        List<Point> result = new LinkedList<>();
        List<Point> pointListTrans = new LinkedList<>();

        for (Point c : coor) {
            pointListTrans.add(new Point(c.getX() / Constant.K, c.getY() / Constant.K));
        }

        for (Point p : pointListTrans) {
            result.add(new Point(p.getX() + ori.getX(), p.getY() + ori.getY()));
        }

        return result;
    }

    /**
     * wps84转换成gcj02
     *
     * @param gps wgs84点列
     * @return gcj02点列
     */
    public static List<Point> wgs84ToGcj02(List<? extends Point> gps) {
        List<Point> result = new LinkedList<>();

        for (Point p : gps) {
            result.add(new Point(CoordinateTransformAlgorithms.transformWGS84ToGCJ02(p.getX(), p.getY())));
        }

        return result;
    }

    /**
     * gcj02转换成wgs84
     *
     * @param gps gcj02点列
     * @return wgs84点列
     */
    public static List<Point> gcj02ToWgs84(List<? extends Point> gps) {
        List<Point> result = new LinkedList<>();

        for (Point p : gps) {
            result.add(new Point(CoordinateTransformAlgorithms.transformGCJ02ToWGS84(p.getX(), p.getY())));
        }

        return result;
    }

}
