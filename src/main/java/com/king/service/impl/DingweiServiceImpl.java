package com.king.service.impl;

import com.king.alg.*;
import com.king.constant.Constant;
import com.king.pojo.Point;
import com.king.pojo.PointList;
import com.king.pojo.TurnFitExtendResult;
import com.king.service.DingweiServiceI;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
public class DingweiServiceImpl implements DingweiServiceI {

    @Override
    public TurnFitExtendResult turnFitExtend(PointList pointList) {
        return TurnFitExtendAlgorithms.turnFitExtend(pointList.getPointList(), Constant.EXTEND_DISTANCE, Constant.DISTANCE_TWO_POINT, Constant.MAX_WEIGHT);
    }

    @Override
    public Boolean isInner(PointList pointList, Point point) {
        return PolygonAlgorithms.isInner(pointList.getPointList(), point);
    }

    @Override
    public PointList wgs84ToGcj02(PointList pointList) {
        return new PointList(GPSAlgorithms.wgs84ToGcj02(pointList.getPointList()));
    }

    @Override
    public PointList gcj02ToWgs84(PointList pointList) {
        return new PointList(GPSAlgorithms.gcj02ToWgs84(pointList.getPointList()));
    }

    @Override
    public PointList multiAvg(List<PointList> pointLists) {
        List<List<Point>> p = new LinkedList<>();
        pointLists.forEach(o -> p.add(o.getPointList()));
        return new PointList(MultiFitAlgorithms.multiAvg(p));
    }

    @Override
    public Double getDistanceByPointAndPolygon(Point point, PointList pointList) {
        Point midPoint = PolygonAlgorithms.middleDistancePoint2Polygon(pointList.getPointList(), point);

        return CaculateDistance.getDistanceByTwoCoordinate(point.getX(), point.getY(), midPoint.getX(), midPoint.getY());
    }
}
