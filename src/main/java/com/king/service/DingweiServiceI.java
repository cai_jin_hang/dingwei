package com.king.service;

import com.king.pojo.Point;
import com.king.pojo.PointList;
import com.king.pojo.TurnFitExtendResult;

import java.util.List;

public interface DingweiServiceI {
    TurnFitExtendResult turnFitExtend(PointList pointList);

    Boolean isInner(PointList pointList, Point point);

    PointList wgs84ToGcj02(PointList pointList);

    PointList gcj02ToWgs84(PointList pointList);

    PointList multiAvg(List<PointList> pointLists);

    Double getDistanceByPointAndPolygon(Point point, PointList pointList);

}
