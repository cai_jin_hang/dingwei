package com.king.result;

public enum RCode {
    SUCCESS(200, "请求成功"),
    ERROR(500, "请求错误"),
    FORBIDDEN(403, "权限阻止"),
    INVALID_TOKEN(426, "无效token"),
    DATA_INVALID(10001, "无效数据"),
    METHOD_NOT_ALLOWED(10002, "方法无权访问"),
    GET_REMOTE_TOKEN_KEY_ERROR(10003, "无法获取token_key"),
    SENTINEL(10004, "被流控"),
    GATEWAY(10005, "网关错误"),
    UNAUTH(401, "未认证");

    // 10005 网关错误信息

    private int code;
    private String msg;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    RCode(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
