package com.king.pojo;

import java.util.LinkedList;
import java.util.List;

public class Point implements Comparable<Point> {

    // 纬度 latitude
    private double x;

    // 经度 longitude
    private double y;

    private int oriIndex = -1;

    private int equIndex = -1;

    private List<Double> weightList;

    public Point() {

    }

    @Override
    public int compareTo(Point o) {
        return this.oriIndex - o.oriIndex;
    }

    public int getOriIndex() {
        return oriIndex;
    }

    public void setOriIndex(int oriIndex) {
        this.oriIndex = oriIndex;
    }

    public int getEquIndex() {
        return equIndex;
    }

    public void setEquIndex(int equIndex) {
        this.equIndex = equIndex;
    }

    public List<Double> getWeightList() {
        return weightList;
    }

    public void setWeightList(List<Double> weightList) {
        this.weightList = weightList;
    }

    public void addWeight(double w) {
        if (this.weightList == null) {
            this.weightList = new LinkedList<>();
        }
        this.weightList.add(w);
    }

    public double getAvgWeight() {
        if (weightList == null || weightList.isEmpty()) {
            return 0;
        }

        double result = 0;

        for (double w : weightList) {
            result += w;
        }

        return result / weightList.size();
    }

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public Point(double[] xy) {
        this.x = xy[0];
        this.y = xy[1];
    }

    public Point(double x, double y, int oriIndex) {
        this(x, y);
        this.oriIndex = oriIndex;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double[] getData() {
        return new double[]{x, y};
    }

    @Override
    public String toString() {
        return "Point{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}
