package com.king.pojo;

import java.util.List;

public class TurnFitExtendResult {

    /**
     * 拐点索引
     */
    private List<Integer> turn;

    /**
     * 拟合点列
     */
    private List<Point> fit;

    /**
     * 外扩点列
     */
    private List<Point> extend;

    public List<Integer> getTurn() {
        return turn;
    }

    public void setTurn(List<Integer> turn) {
        this.turn = turn;
    }

    public List<Point> getFit() {
        return fit;
    }

    public void setFit(List<Point> fit) {
        this.fit = fit;
    }

    public List<Point> getExtend() {
        return extend;
    }

    public void setExtend(List<Point> extend) {
        this.extend = extend;
    }
}
