package com.king.pojo;

public class IsInnerParam {

    /**
     * 点列，建筑物的外扩点列
     */
    private PointList pointList;

    /**
     * 点，人的经纬度
     */
    private Point point;

    public PointList getPointList() {
        return pointList;
    }

    public void setPointList(PointList pointList) {
        this.pointList = pointList;
    }

    public Point getPoint() {
        return point;
    }

    public void setPoint(Point point) {
        this.point = point;
    }
}
