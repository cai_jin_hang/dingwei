package com.king.pojo;

import java.util.List;

public class PointList {
    private List<Point> pointList;

    public PointList() {

    }

    public PointList(List<Point> pointList) {
        this.pointList = pointList;
    }

    public List<Point> getPointList() {
        return pointList;
    }

    public void setPointList(List<Point> pointList) {
        this.pointList = pointList;
    }
}
