package com.king.constant;

public class Constant {

    /**
     * 放大常数
     */
    public static final double K = 100000;

    /**
     * 外扩距离
     */
    public static final double EXTEND_DISTANCE = 10;

    /**
     * 等距取点的两点最小距离
     */
    public static final double DISTANCE_TWO_POINT = 1;

    /**
     * Douglas-Peucker 最大权重
     */
    public static final double MAX_WEIGHT = 0.1;

}
