package com.king.controller;

import com.king.pojo.GetDistanceParam;
import com.king.pojo.IsInnerParam;
import com.king.pojo.PointList;
import com.king.pojo.TurnFitExtendResult;
import com.king.result.R;
import com.king.result.RCode;
import com.king.service.DingweiServiceI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@Controller
@RequestMapping("/dingwei")
public class DingweiController {

    @Autowired
    private DingweiServiceI dingweiService;

    /**
     * 拐点，拟合，外扩
     *
     * @param pointList 原始点列
     * @return 拐点，拟合，外扩
     */
    @ResponseBody
    @PostMapping(value = "/turnFitExtend")
    public R<TurnFitExtendResult> turnFitExtend(@RequestBody PointList pointList) {
        R<TurnFitExtendResult> r = new R<>(RCode.SUCCESS);
        r.setData(dingweiService.turnFitExtend(pointList));
        return r;
    }

    /**
     * 判断点是否在多边形内部
     *
     * @return 点是否在多边形内部
     */
    @ResponseBody
    @PostMapping("/isInner")
    public R<Boolean> isInner(@RequestBody IsInnerParam param) {
        R<Boolean> r = new R<>(RCode.SUCCESS);
        r.setData(dingweiService.isInner(param.getPointList(), param.getPoint()));
        return r;
    }

    /**
     * wgs84转gcj02
     *
     * @param pointList wgs84点列
     * @return gcj02点列
     */
    @ResponseBody
    @RequestMapping("/wgs84ToGcj02")
    public R<PointList> wgs84ToGcj02(@RequestBody PointList pointList) {
        R<PointList> r = new R<>(RCode.SUCCESS);
        r.setData(dingweiService.wgs84ToGcj02(pointList));
        return r;
    }

    /**
     * gcj02转wgs84
     *
     * @param pointList gcj02点列
     * @return wgs84点列
     */
    @ResponseBody
    @RequestMapping("/gcj02ToWgs84")
    public R<PointList> gcj02ToWgs84(@RequestBody PointList pointList) {
        R<PointList> r = new R<>(RCode.SUCCESS);
        r.setData(dingweiService.gcj02ToWgs84(pointList));
        return r;
    }

    /**
     * 根据多次重复测量得出拟合的结果
     *
     * @param pointLists 多个点列
     * @return 最终的点列
     */
    @ResponseBody
    @RequestMapping("/multiAvg")
    public R<PointList> multiAvg(@RequestBody List<PointList> pointLists) {
        R<PointList> r = new R<>(RCode.SUCCESS);
        r.setData(dingweiService.multiAvg(pointLists));
        return r;
    }

    /**
     * 求点到多边形的距离
     *
     * @param param
     * @return
     */
    @ResponseBody
    @RequestMapping("/getDistanceByPointAndPolygon")
    public R<Double> getDistanceByPointAndPolygon(@RequestBody GetDistanceParam param) {
        R<Double> r = new R<>(RCode.SUCCESS);
        r.setData(dingweiService.getDistanceByPointAndPolygon(param.getPoint(), param.getPointList()));
        return r;
    }

}
